#!/usr/bin/python

import sys, os, re, argparse, subprocess as sp
from math import log10, floor
from collections import Counter

# Global switch, specifies whether or not to apply demangling
do_demangling = True

def demangle(function):
    '''Demangle CUDA/C++ function name using c++filt command
    is do_demangling is set to True and return the result.
    If do_demangling is set to False, don't apply and filter
    and return the input.'''

    if do_demangling:
        # Call c++filt program
        proc = sp.Popen(['c++filt', function], stdin=sp.PIPE, stdout=sp.PIPE)
        out, _ = proc.communicate()
        demangled = out.strip().decode()
        return re.match('(void )?([^\(]+)\(.*\)', demangled).groups()[1]
    else:
        return function

class Executable(object):
    def __init__(self, fname):
        '''Initializes the object by reading the output of cuobjdump.
        The SASS code is interpreted, the kernels stored separately with
        the instructions converted into an internal representation.

        fname : path to the executable (or object file) to inspect'''

        # Call cuobjdump
        proc = sp.Popen(['cuobjdump', '-sass', fname], stdout=sp.PIPE, stderr=sp.PIPE)
        stdout, _ = proc.communicate()

        # Interpret SASS code
        f = None
        kernels = {}
        current = []
        for l in stdout.decode().split('\n'):

            # Recognize new kernel
            if l.startswith('		Function : '):
                f = demangle(l[13:])
                continue

            # Recognize end of kernel
            if l.startswith('		..........'):
                kernels[f] = current
                f = None
                current = []

            # Save the line in the current kernel
            if f is not None and '.headerflags' not in l:
                l = l.strip()
                lid = int(l[2:6], base=16)
                predicate = l[8:17].strip()
                predicate = predicate if predicate != '' else None
                instrend = l[17:].index(' ') + 17
                paramsend = l[17:].index(';') + 17
                instructionname = l[17:instrend]
                if instructionname.endswith(';'):
                    instructionname = instructionname[:-1]
                parameters = tuple([i.strip() for i in l[instrend+1:paramsend].split(',')])
                if len(parameters) == 0 or parameters[0] == '':
                    parameters = ()

                current.append((lid, predicate, instructionname, parameters))

        self._kernels = kernels


    def instructionsUsage(self, kernel):
        '''Return list of tuples instruction-usage with the number of
        times each instruction is called, sorted by decreasing number of
        usages.

        kernel : name of the kernel to inspect '''

        return sorted(
                list(Counter([i[2] for i in self._kernels[kernel]]).items()),
                key = lambda i : (-i[1], i[0]),
                reverse = False
               )


    @property
    def kernels(self):
        return self._kernels


description = \
'''Inspect usage of instructions in CUDA kernel.

desass opens a CUDA file (an object file or an executable)
and interprets the contained kernels.  By providing one or more
"-k" switches, the user can select the kernel to inspect:

    desass my_executable -k mykernel1 -k mykernel2

If no "-k" switch is provided, desass will give a list of the
existing kernels and the user can select the desired ones by
entering them in the standard input when requested.'''

if __name__ == '__main__':
    parser = argparse.ArgumentParser('desass', description=description,
            epilog='Contact: andrea.arteaga@env.ethz.ch')
    parser.add_argument('exe', help='Executable containing CUDA kernels')
    parser.add_argument('-k', '--kernel', help='Name of the kernel to inspect', action='append')
    parser.add_argument('-m', '--mangled', help='Do not demangle function names', action='store_true')
    args = parser.parse_args()

    do_demangling = not args.mangled

    print('Reading', args.exe, 'file...', end=' ')
    sys.stdout.flush()
    exe = Executable(args.exe)
    print('DONE\n')
    sys.stdout.flush()

    # No function name provided: print list of existing kernels
    if args.kernel is None:
        funcs = sorted(exe.kernels.keys())
        nl = floor(log10(len(funcs)))+1

        # Print kernels
        print('Available kernels:')
        for i, f in enumerate(funcs):
            print(' [{0:{1:d}d}] {2}'.format(i, nl, f))
        print()

        # User input
        indices = input('Enter index of function (separate multiple indices with commas): ').split(',')
        print()
        kernels = [funcs[int(idx)] for idx in indices]

    # List of funcitons provided: use that
    else:
        kernels = args.kernel

    # Pretty print summary of arguments of given functions
    for kernel in kernels:
        instructions = exe.instructionsUsage(kernel)
        nl = max(len(i[0]) for i in instructions)

        print('', (4+len(kernel))*'=')
        print(' |', kernel, '|')
        print(' |', (2+len(kernel))*'-', '|', sep='')
        spacelen = len(kernel)-nl-8
        space1 = min(5, spacelen//3)
        space2 = spacelen - space1
        for instr, count in instructions:
            print('', '|', space1*' ', '{0:>{1:d}s} : {2:3d}'.format(instr, nl, count), space2*' ', '|')

        print('', (4+len(kernel))*'=')
        print()

