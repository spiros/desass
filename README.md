# README

Desass is a tool designed for the inspection of CUDA kernels. Desass will read
a file containing CUDA kernels (e.g. an executable) through `cubojdump` and
interpret the SASS code contained there.

## Features

desass is currently able to:

 * Read CUDA executables
 * Locate all CUDA kernels in these files
 * Convert their SASS code into an internal representation
 * Produce some analysis on the kernels (currently just the frequency of the
   instructions used in the kernels)

## Usage of the script

Call it with

    desass my_executable -k my_kernel -k my_other_kernel

to inspect the content of the two specified kernels in the given executable.
    desass will produce an output which looks like this:

    Reading my_executable file... DONE

     ==============================
     | my_kernel                  |
     |----------------------------|
     |              FADD :  16    |
     |               S2R :   6    |
     |              IADD :   5    |
     |      ISETP.EQ.AND :   5    |
     |              FMUL :   4    |
     |              IMAD :   4    |
     |             SHL.W :   4    |
     |               SHR :   4    |
     |              EXIT :   3    |
     |   IMAD.U32.U32.HI :   3    |
     |            ISCADD :   3    |
     |              ISUB :   3    |
     |           LOP.AND :   3    |
     |      BAR.RED.POPC :   2    |
     |            IADD.X :   2    |
     |       ISET.LT.AND :   2    |
     |              LD.E :   2    |
     |               LDS :   2    |
     |           LDS.128 :   2    |
     |            MOV32I :   2    |
     |               NOP :   2    |
     |           IMAD.HI :   1    |
     |         IMAD.HI.X :   1    |
     |      ISETP.GE.AND :   1    |
     |       ISETP.GE.OR :   1    |
     |      ISETP.GT.AND :   1    |
     |       ISETP.LT.OR :   1    |
     |            ISUB.X :   1    |
     |          LD.E.128 :   1    |
     |               MOV :   1    |
     |               SHL :   1    |
     |          ST.E.128 :   1    |
     |           ST.E.64 :   1    |
     |           STS.128 :   1    |
     ==============================

     ===============================
     | my_other_kernel             |
     |-----------------------------|
     |              FADD :  28     |
     |               S2R :   6     |
     |              IADD :   5     |
     |      ISETP.EQ.AND :   5     |
     |              FMUL :   4     |
     |              IMAD :   4     |
     |             SHL.W :   4     |
     |               SHR :   4     |
     |              EXIT :   3     |
     |   IMAD.U32.U32.HI :   3     |
     |            ISCADD :   3     |
     |              ISUB :   3     |
     |           LOP.AND :   3     |
     |      BAR.RED.POPC :   2     |
     |            IADD.X :   2     |
     |       ISET.LT.AND :   2     |
     |              LD.E :   2     |
     |               LDS :   2     |
     |            MOV32I :   2     |
     |           IMAD.HI :   1     |
     |         IMAD.HI.X :   1     |
     |      ISETP.GE.AND :   1     |
     |       ISETP.GE.OR :   1     |
     |      ISETP.GT.AND :   1     |
     |       ISETP.LT.OR :   1     |
     |            ISUB.X :   1     |
     |          LD.E.128 :   1     |
     |               MOV :   1     |
     |               NOP :   1     |
     |               SHL :   1     |
     |          ST.E.128 :   1     |
     |           ST.E.64 :   1     |
     |           STS.128 :   1     |
     ===============================


You can also not specify the name of the kernels and let desass print a list
of them: just call it with

    desass my_executable

and desass will print a list like this:

    Reading my_executable file... DONE

    Available kernels:
     [ 0] kernel<float, Diffusion1, 32, 14, 4, 4>
     [ 1] kernel<float, Diffusion1_10, 32, 14, 4, 4>
     [ 2] kernel<float, Diffusion1_20, 32, 14, 4, 4>
     [ 3] kernel_noflop<float, Diffusion1, 32, 14, 4, 4>
     [ 4] kernel_noflop<float, Diffusion1_10, 32, 14, 4, 4>
     [ 5] kernel_noflop<float, Diffusion1_20, 32, 14, 4, 4>
     [ 6] kernel_nolds<float, Diffusion1, 32, 14, 4, 4>
     [ 7] kernel_nolds<float, Diffusion1_10, 32, 14, 4, 4>
     [ 8] kernel_nolds<float, Diffusion1_20, 32, 14, 4, 4>
     [ 9] kernel_noload<float, Diffusion1, 32, 14, 4, 4>
     [10] kernel_noload<float, Diffusion1_10, 32, 14, 4, 4>
     [11] kernel_noload<float, Diffusion1_20, 32, 14, 4, 4>
     [12] kernel_nostore<float, Diffusion1, 32, 14, 4, 4>
     [13] kernel_nostore<float, Diffusion1_10, 32, 14, 4, 4>
     [14] kernel_nostore<float, Diffusion1_20, 32, 14, 4, 4>
     [15] kernel_nosts<float, Diffusion1, 32, 14, 4, 4>
     [16] kernel_nosts<float, Diffusion1_10, 32, 14, 4, 4>
     [17] kernel_nosts<float, Diffusion1_20, 32, 14, 4, 4>
     [18] kernel_nosync<float, Diffusion1, 32, 14, 4, 4>
     [19] kernel_nosync<float, Diffusion1_10, 32, 14, 4, 4>
     [20] kernel_nosync<float, Diffusion1_20, 32, 14, 4, 4>

    Enter index of function (separate multiple indices with commas):

Inserting the string `5,12,2` will produce an output similar to the previous,
with the information for the three specified kernels.

## Usage of the API

Desass provides the class `Executable`, which represents a file containing CUDA
kernels. Objects of this class are initialized with a single parameter: the path
to the file to inspect:

    import desass
    exe = desass.Executable('/path/to/my/executable')

The path can be absolute or relative. At initialization time the object will
do the whole interpretation and store the information in the internal data
structure `self.kernels`.

`self.kernels` is an unmutable dictionary containing one entry for each kernel.
Each item has the name of the kernel as key and the interpreted SASS code as
value, i.e., as list of tuples, each tuple containing the following information:

 0. The instruction ID (e.g. `00b8`) as integer
 1. The predicate (e.g. `@P0`) as string if any, `None` otherwise
 2. The name of the instruction (e.g. `LD.E.128`) as string
 3. The list of parameters (e.g. `('R12', '[R4]')`) as tuple of strings

Examples:

    print(len(exe.kernels['my_kernel']))
    # Prints 109

    print(exe.kernels['my_kernel'][36:40])
    # Prints:
    #   [(288, None, 'ISETP.GT.AND', ('P0', 'PT', 'R18', '0xd', 'PT')),
    #    (296, None, 'LD.E.128', ('R4', '[R8]')),
    #    (304, None, 'IMAD', ('R18', 'R12', '0xe', 'R18')),
    #    (312, '@!P1', 'LDS', ('R17', '[R16+-0x4]'))]

